<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Traits\HasRolesAndPermissions;


class User extends Authenticatable implements JWTSubject
{
    use SoftDeletes, HasRolesAndPermissions;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'login', 'password'
    ];

    protected $hidden = [
        'password'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'author_id', 'id');
    }

    public function feedbacks()
    {
        return $this->hasMany(Feedback::class, 'author_id', 'id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
