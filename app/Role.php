<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name'
    ];

    public const adminRoleName = 'Admin';
    public const adminRoleSlug = 'admin';
    public const defaultRoleName = 'User';
    public const defaultRoleSlug = 'user';

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'roles_permissions');
    }

    public static function getDefaultRole()
    {
        return self::firstWhere('name', '=', self::defaultRoleName);
    }

    public static function getAdminRole()
    {
        return self::firstWhere('name', '=', self::adminRoleName);
    }
}
