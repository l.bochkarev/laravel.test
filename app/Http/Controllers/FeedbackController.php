<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function create(Request $request)
    {
        $request->validate([
            'product_id' => 'integer|required',
            'mark' => 'integer|required',
            'description' => 'string|required'
        ]);

        $feedback = new Feedback([
            'product_id' => $request->input('product_id'),
            'author_id' => auth()->user()->id,
            'mark' => $request->input('mark'),
            'description' => $request->input('description')
        ]);
        $feedback->save();

        return redirect()->back()->with('success', 'Feedback added!');
    }
}
