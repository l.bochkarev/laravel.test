<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function login()
    {
        return view('users.login');
    }

    public function postLogin(Request $request)
    {
        $request->validate([
            'login' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('login', 'password');

        if (auth()->attempt($credentials)) {
            return redirect('/')->with('success', 'Successful auth!');
        }

        return redirect()->back()->withErrors('Invalid credentials.');
    }

    public function register()
    {
        return view('users.register');
    }

    public function postRegister(Request $request)
    {
        $request->validate([
            'login' => 'required|unique:users',
            'password' => 'required|min:6',
        ]);

        $role = Role::getDefaultRole();
        if (empty($role)) {
            return redirect()->back()->withErrors('Role on default not found!');
        }
        $user = new User([
            'login' => $request->input('login'),
            'password' => Hash::make($request->input('password'))
        ]);
        $user->save();
        $user->roles()->attach($role->id);

        return $this->postLogin($request);
    }

    public function logout()
    {
        Auth()->logout();
        Session()->flush();
        return redirect('users/login');
    }
}
