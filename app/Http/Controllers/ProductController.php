<?php

namespace App\Http\Controllers;

use App\Role;
use DB;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\User;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:' . Role::adminRoleSlug, ['except' => [
            'index',
            'show',
        ]]);;
    }

    public function index(Request $request)
    {
        $request->validate([
            'sortBy' => 'string',
            'category_id' => 'int'
        ]);

        $products = Product::select(
            'products.*',
            'users.login as author_name'
        )
            ->join('users', 'products.author_id', '=', 'users.id')
            ->leftJoin('product_categories', 'products.id', '=', 'product_categories.product_id')
            ->leftJoin('feedbacks', 'products.id', '=', 'feedbacks.product_id')
            ->selectRaw('count(DISTINCT feedbacks.id) as feedbacks_count, AVG(feedbacks.mark) as average_mark')
            ->groupBy('products.id');

        $filterByCategoryId = $request->input('category_id', 0);
        if ($filterByCategoryId !== 0) {
            $products = $products->where('product_categories.category_id', '=', $filterByCategoryId);
        }

        $sortBy = $request->input('sortBy', 'id');
        $products = $products->orderBy($sortBy, 'DESC');

        // 6 - наиболее воспринимаемое количество, больше выглядит громоздко
        return view('products.index')->with('products', $products->paginate(6));
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'string|required|max:255',
            'description' => 'string|required|max:1000',
            'image' => 'required|max:1000|mimes:jpeg,jpg,png',
            'categories_ids' => 'array|required|distinct'
        ]);

        $image_name = uniqid() . "." . $request->file('image')->extension();
        $request->file('image')->storePubliclyAs('public', $image_name);

        $product = new Product([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'image_name' => $image_name,
            'author_id' => auth()->id()
        ]);
        $product->save();

        foreach ($request->input('categories_ids') as $category_id) {
            $product->categories()->attach($category_id);
        }

        return redirect('/products')->with('success', 'Product added!');
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('products.show', compact('product'));
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('products.edit', compact('product'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'string|required|max:255',
            'description' => 'string|required|max:1000',
            'image' => 'max:1000|mimes:jpeg,jpg,png',
            'categories_ids' => 'array|distinct'
        ]);

        $product = Product::findOrFail($id);
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->save();

        $new_categories_ids = $request->input('categories_ids');
        if ($request->exists('categories_ids') && count($new_categories_ids) !== 0) {
            foreach ($product->categories as $category) {
                if (in_array($category->id, $new_categories_ids) === false) {
                    $product->categories()->detach($category->id);
                }
            }
            foreach ($new_categories_ids as $category_id) {
                if (is_null($product->categories->find($category_id))) {
                    $product->categories()->attach($category_id);
                }
            }
        }

        if ($request->exists('image')) {
            $request->file('image')->storePubliclyAs("public", $product->image_name);
        }

        return redirect('/products')->with('success', 'Product updated!');
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        $product->delete();
        unlink(storage_path("app/public/{$product->image_name}"));

        return redirect('/products')->with('success', 'Product deleted!');
    }
}
