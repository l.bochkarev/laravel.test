<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Validator;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'role:' . Role::adminRoleSlug], ['except' => [
            'index',
            'show',
            'categories'
        ]]);
    }

    public function index()
    {
        $products = Product::select(
            'products.*',
            'users.login as author_name'
        )
            ->join('users', 'products.author_id', '=', 'users.id')
            ->leftJoin('product_categories', 'products.id', '=', 'product_categories.product_id')
            ->leftJoin('categories', 'product_categories.category_id', '=', 'categories.id')
            ->leftJoin('feedbacks', 'products.id', '=', 'feedbacks.product_id')
            ->selectRaw('count(DISTINCT feedbacks.id) as feedbacks_count, AVG(feedbacks.mark) as average_mark')
            ->groupBy('products.id')
            ->selectRaw('GROUP_CONCAT(DISTINCT categories.id  separator \',\') as categories_ids')
            ->get();

        return BaseController::sendResponse(
            $products,
            'Products in data.',
            'Product information received!',
            200
        );
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|required|max:255',
            'description' => 'string|required|max:1000',
            'image' => 'required|max:1000|mimes:jpeg,jpg,png',
            'categories_ids' => 'array|required',
            'categories_ids.*' => 'integer|required|distinct|exists:categories,id'
        ]);

        if ($validator->fails()) {
            return BaseController::sendError(
                $validator->errors(),
                'Errors in data.',
                'New product did not pass verification!',
                400
            );
        }

        $image_name = uniqid() . "." . $request->file('image')->extension();
        $request->file('image')->storePubliclyAs("public", $image_name);

        $product = new Product([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'image_name' => $image_name,
            'author_id' => auth()->id()
        ]);
        $product->save();

        foreach ($request->input('categories_ids') as $category_id) {
            $product->categories()->attach($category_id);
        }

        return BaseController::sendResponse(
            $product,
            'Product created.',
            'Product added!',
            201
        );
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        $product->author_name = $product->author->login;
        $product->categories = $product->categories()->get();
        $product->feedbacks = $product->feedbacks()->get();
        return BaseController::sendResponse(
            $product,
            'Product information in data.',
            'Product information loaded!',
            200
        );
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|max:255',
            'description' => 'string|max:1000',
            'image' => 'max:1000|mimes:jpeg,jpg,png',
            'categories_ids.*' => 'array|distinct|exists:categories,id'
        ]);

        if ($validator->fails()) {
            return BaseController::sendError(
                $validator->errors(),
                'Errors in data.',
                'Update product did not pass verification!',
                400
            );
        }

        $product = Product::findOrFail($id);
        if ($request->exists('name')) {
            $product->name = $request->input('name');
        }
        if ($request->exists('description')) {
            $product->description = $request->input('description');
        }
        $product->save();

        $new_categories_ids = $request->input('categories_ids');
        if ($request->exists('categories_ids') && count($new_categories_ids) !== 0) {
            foreach ($product->categories as $category) {
                if (in_array($category->id, $new_categories_ids) === false) {
                    $product->categories()->detach($category->id);
                }
            }
            foreach ($new_categories_ids as $category_id) {
                if (is_null($product->categories->find($category_id))) {
                    $product->categories()->attach($category_id);
                }
            }
        }

        if ($request->exists('image')) {
            $request->file('image')->storePubliclyAs("public", $product->image_name);
        }

        return BaseController::sendResponse(
            null,
            'Product updated.',
            'Product information updated!',
            201
        );
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        $product->delete();
        unlink(storage_path("app/public/{$product->image_name}"));

        return BaseController::sendResponse(
            null,
            'Delete product.',
            'Product deleted!',
            200
        );
    }

    public function categories()
    {
        return BaseController::sendResponse(
            Category::all(),
            'Categories in data.',
            'Categories list loaded!',
            200
        );
    }
}
