<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public static function sendResponse($data, $devMessage, $userMessage, $code = 200)
    {
        $response = [
            'success' => true,
            'dev_message' => $devMessage,
            'user_message' => $userMessage,
            'data' => $data
        ];

        return response()->json($response, $code);
    }

    public static function sendError($errorMessages, $devMessage, $userMessage, $code = 404)
    {
        $response = [
            'success' => false,
            'dev_message' => $devMessage,
            'user_message' => $userMessage,
            'data' => $errorMessages
        ];

        return response()->json($response, $code);
    }
}
