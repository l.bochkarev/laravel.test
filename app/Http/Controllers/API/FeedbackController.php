<?php

namespace App\Http\Controllers\API;

use App\Feedback;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Validator;

class FeedbackController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function create(Request $request, $product_id)
    {
        Product::findOrFail($product_id);

        $validator = Validator::make($request->all(), [
            'mark' => 'integer|required|min:0|max:5',
            'description' => 'string|required|max:400'
        ]);

        if ($validator->fails()) {
            return BaseController::sendError(
                $validator->errors(),
                'Errors in data.',
                'New feedback did not pass verification!',
                400
            );
        }

        $feedback = new Feedback([
            'product_id' => $product_id,
            'author_id' => auth()->user()->id,
            'mark' => $request->input('mark'),
            'description' => $request->input('description')
        ]);
        $feedback->save();

        return BaseController::sendResponse(
            null,
            'Commented.',
            'Feedback added!',
            200
        );
    }
}
