<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => [
            'login',
            'register'
        ]]);
    }

    public function login()
    {
        $credentials = request()->only(['login', 'password']);

        $token = JWTAuth::attempt($credentials);

        if ($token === false) {
            return BaseController::sendError(
                null,
                'Invalid credentials.',
                'Incorrect login or password!',
                401
            );
        }

        return BaseController::sendResponse(
            $this->wrapToken($token),
            'Ok. Info about token in data.',
            'Authorized success!',
            200
        );
    }

    public function register()
    {
        $validator = Validator::make(request()->only(['login', 'password']), [
            'login' => 'required|unique:users',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return BaseController::sendError(
                $validator->errors(),
                'Errors in data.',
                'Login or password did not pass verification!',
                400
            );
        }

        $role = Role::getDefaultRole();
        if (empty($role)) {
            return BaseController::sendError(
                null,
                'Role on default not set in model Role and/or not found in database.',
                'Registration is not possible, in database missing user role!',
                500
            );
        }
        $user = new User([
            'login' => request()->input('login'),
            'password' => Hash::make(request()->input('password'))
        ]);
        $user->save();
        $user->roles()->attach($role->id);

        return BaseController::sendResponse(
            null,
            'Registered.',
            'Successfully registration!',
            201
        );
    }

    public function me()
    {
        return BaseController::sendResponse(
            auth()->user(),
            'User in data.',
            'Information about you received successful!',
            200
        );
    }

    public function logout()
    {
        auth()->logout();

        return BaseController::sendResponse(
            null,
            'Logged out.',
            'Successfully logged out!',
            200
        );
    }

    public function refresh()
    {
        $token = auth()->refresh();

        return BaseController::sendResponse(
            $this->wrapToken($token),
            'Refreshed token in data.',
            'Welcome back!',
            200
        );
    }

    protected function wrapToken($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ];
    }
}
