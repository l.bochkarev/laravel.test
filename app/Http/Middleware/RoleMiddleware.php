<?php

namespace App\Http\Middleware;

use App\Http\Controllers\API\BaseController;
use Closure;

class RoleMiddleware
{
    public function handle($request, Closure $next, $role, $permission = null)
    {
        if (
            auth()->user()->hasRole($role) === false
            ||
            $permission !== null && auth()->user()->hasPermissionTo($permission) === false
        ) {
            if ($request->expectsJson()) {
                return BaseController::sendError(
                    null,
                    "Need role: {$role}, permission: {$permission}.",
                    'You don\'t have permission!',
                    403
                );
            }
            abort(403);
        }
        return $next($request);
    }
}
