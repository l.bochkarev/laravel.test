<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', '404') - Network Shop</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .content {
            text-align: center;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .user-info {
            text-align: left;
            padding: 10px;
        }
    </style>
</head>

<body>

<div class="user-info">
    @if (auth()->check())
        Hello, {{ auth()->user()->login }}
        <a href="{{ route('users.logout') }}">Logout</a>
    @endif
    @yield('user-info-additional')
</div>

<div class="content flex-center">
    <div>
        @yield('content')
    </div>
</div>

<div class="flex-center">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>

</body>
<br/><br/>
</html>
