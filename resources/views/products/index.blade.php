@extends('layout')
@section('title', 'Main Page')
@section('user-info-additional')
    @guest
        <a href="{{ route('users.login') }}">Login</a>
    @endguest
    @role('admin')
    <a href="{{ route('products.create')}}">Add new product</a>
    @endrole
@endsection
<style>
    td a {
        display: block;
        padding: 6px;
    }

    td {
        text-align: center;
    }

    select {
        border: 1px;
        background: transparent;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        background: none;
        padding: 5px;
        font-family: inherit;
    }

    .pagination li {
        display: inline-block;
    }

    .pagination li + li {
        margin-left: 1rem;
    }

    .pagination a {
        text-decoration: none;
        padding: 0.2rem 0.4rem;
        color: #000000;
        border: 1px solid #000000;
        border-radius: 2px;
    }
</style>
@section('content')
    <div class="row">
        <h1>Our products</h1>
        <table>
            <thead>
            <tr>
                <td>
                    <a href="{{ route('products.index', ['sortBy' => 'id']) }}">ID</a>
                </td>
                <td>
                    <a href="{{ route('products.index', ['sortBy' => 'name']) }}">Name</a>
                </td>
                <td>Description</td>
                <td>Image</td>
                <td>
                    <a href="{{ route('products.index', ['sortBy' => 'author_name']) }}">Author</a>
                </td>
                <td>
                    <a href="{{ route('products.index', ['sortBy' => 'feedbacks_count']) }}">Feedbacks</a>
                </td>
                <td>
                    <a href="{{ route('products.index', ['sortBy' => 'average_mark']) }}">Mark</a>
                </td>
                <td>Category</td>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td class="field">{{$product->id}}</td>
                    <td><a href="{{ route('products.show', $product->id) }}">{{ $product->name }}</a></td>
                    <td>{{ mb_substr($product->description, 0, 9) }}...</td>
                    <td><img height="32px" width="32px"
                             src="{{ asset('storage/' . $product->image_name) }}"></td>
                    <td>{{ $product->author_name }}</td>
                    <td>{{ $product->feedbacks_count }}</td>
                    <td>{{ round($product->average_mark, 1) }}</td>
                    <td>
                        <ul>
                            @foreach($product->categories as $category)
                                <li>{{ $category->name }}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
    <form>
        <br/>Filter by category:
        @if (empty(Request::input('category_id')))
            <select name="category_id" onchange="this.form.submit()">
                <option selected disabled value=0>None</option>
                @foreach(App\Category::all() as $category)
                    <option value={{ $category->id }}> {{ $category->name }} </option>
                @endforeach
            </select>
        @else
            {{App\Category::findOrFail(Request::input('category_id'))->name}}
            <a href="{{ route('products.index') }}">Reset</a>
        @endif
    </form>
    <br/><br/>
    {{ $products->withQueryString()->render() }}
@endsection
