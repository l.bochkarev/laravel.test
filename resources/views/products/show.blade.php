@extends('layout')
@section('title', $product->name)
<style>
    .product-info {
        display: inline-block;
        margin-left: 5%;
        margin-right: 5%;
    }
</style>
@section('content')
    <h1>Detail Information</h1>
    <div class="product-info">
        <h2>{{ $product->name }}</h2>
        <p>
            <img height="190px" width="190px" src="{{ asset('storage/'. $product->image_name) }}" alt="Illustration"
                 align="left" vspace="5" hspace="5">
            {{ $product->description }}
        </p>
    </div>
    <br/>
    <div>
        <strong>Average mark:</strong> {{ round($product->feedbacks()->avg('mark'), 1) }}<br/>
        <strong>Author:</strong> {{ $product->author()->first()->login }}<br/>
        <strong>Categories:</strong><br/>
        {{ implode(', ', $product->categories->pluck('name')->all()) }}<br/>
        <br/>
        @guest
            <a href="{{ route('users.login') }}">Login</a><br/>
            <br/>
        @endguest
        @role('admin')
        <a href="{{ route('products.edit', $product->id) }}">Edit</a><br/>
        <br/>
        <form action="{{ route('products.destroy', $product->id) }}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit">Delete</button>
        </form>
        <br/>
        @endrole
        <a href="{{ route('products.index') }}">Back to list products</a>
    </div>
    <div>
        <h3>Feedbacks</h3>
        @foreach($product->feedbacks as $feedback)
            <strong>{{ $feedback->author->login }}</strong> - mark: {{ $feedback->mark }}</br>
            Comment: {{ $feedback->description }}</br>
            </br>
        @endforeach
        <form method="post" action="{{ route('feedbacks.create') }}">
            @csrf
            <div>
                <label for="mark">Mark:</label>
                <input required="required" min="0" max="5" type="number" name="mark"/>
            </div>
            <div>
                <label for="description">Description:</label><br/>
                <input maxlength=400 required="required" type="text" name="description"/>
            </div>
            <input type="hidden" name="product_id" value="{{ $product->id }}"/>
            <br/>
            <button type="submit">Comment</button>
        </form>
    </div>
@endsection
