@extends('layout')
@section('title', 'Edit ' . $product->name)
@section('content')
    <h1>Update product</h1>
    <div class="row">
        <form method="post" enctype="multipart/form-data" action="{{ route('products.update', $product->id) }}">
            @method('PATCH')
            @csrf
            <div>
                <label for="name">Name:</label>
                <input maxlength=255 required="required" type="text" name="name" value={{ $product->name }} />
            </div>
            <div>
                <label for="image">Image:</label>
                <input type="file" name="image"/>
            </div>
            <div>
                <label for="description">Description:</label></br>
                <textarea maxlength=1000 cols=60 rows=13 required="required" type="text"
                          name="description">{{ $product->description }}</textarea>
            </div>
            <div>
                <label for="categories_ids">Categories:</label></br>
                <select multiple name="categories_ids[]">
                    <option selected disabled>Not modify</option>
                    <?php
                    use App\Category;
                    $categories = Category::all();
                    $categories->map(function ($category) {
                        echo "<option value=\"{$category->id}\">{$category->name}</option>";
                    });
                    ?>
                </select>
            </div>
            </br>
            <button type="submit">Update</button>
        </form>
    </div>
    <br/>
    <a href="{{ route('products.show', $product->id) }}">Back to product</a>
@endsection
