@extends('layout')
@section('title', 'New product')
@section('content')
    <h1>New product</h1>
    <div class="row">
        <form method="post" enctype="multipart/form-data" action="{{ route('products.store') }}">
            @csrf
            <div>
                <label for="name">Name:</label>
                <input maxlength=255 required="required" type="text" name="name"/>
            </div>
            <div>
                <label for="image">Image:</label>
                <input required="required" type="file" name="image"/>
            </div>
            <div>
                <label for="description">Description:</label></br>
                <textarea maxlength=1000 cols=60 rows=13 required="required" type="text" name="description"></textarea>
            </div>
            <div>
                <label for="categories_ids">Categories:</label></br>
                <select multiple name="categories_ids[]">
                    <?php
                    use App\Category;
                    $categories = Category::all();
                    $categories->map(function ($category) {
                        echo "<option value=\"{$category->id}\">{$category->name}</option>";
                    });
                    ?>
                </select>
            </div>
            <br/>
            <button type="submit">Add product</button>
        </form>
    </div>
    <br/>
    <a href="{{ route('products.index') }}">Back to list products</a>
@endsection
