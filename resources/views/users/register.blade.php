@extends('layout')
@section('title', 'Registration')
@section('content')
    <div class="row">
        <h1>Registration</h1>
        @if (auth()->check())
            You already register
        @else
            <form method="POST" action="{{ route('users.postRegister') }}">
                @csrf
                <div>
                    <label for="login">Login</label><br/>
                    <input name="login" type="text" placeholder="Unique login"/>
                </div>
                <div>
                    <label for="password">Password:</label><br/>
                    <input type="password" name="password" placeholder="Enter password"/>
                </div>
                <br/>
                <button type="submit">Create account</button>
            </form>
            <br/>
            <a href="{{ route('users.login') }}">Login</a>
        @endif
        <br/><br/>
        <a href="{{ route('/') }}">Back to main page</a>
@endsection
