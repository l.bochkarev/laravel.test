@extends('layout')
@section('title', 'Login')
@section('content')
    <div class="row">
        <h1>Authorization</h1>
        @if (auth()->check())
            You already login
        @else
            <form method="POST" action="{{ route('users.postLogin') }}">
                @csrf
                <div>
                    <label for="login">Login</label><br/>
                    <input name="login" type="text" placeholder="Enter login"/>
                </div>

                <div>
                    <label for="password">Password:</label><br/>
                    <input type="password" name="password" placeholder="Enter password"/>
                </div>
                <br/>
                <button type="submit">Sign In</button>
            </form>
            <br/>
            <a href="{{ route('users.register') }}">Registration</a>
        @endif
        <br/><br/>
        <a href="{{ route('/') }}">Back to main page</a>
@endsection
