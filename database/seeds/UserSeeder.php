<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;
use App\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        $admin = Role::where('slug', 'admin')->first();

        $createProducts = Permission::where('slug', 'create-products')->first();
        $updateProducts = Permission::where('slug', 'update-products')->first();
        $deleteProducts = Permission::where('slug', 'delete-products')->first();

        $user1 = new User();
        $user1->login = 'admin';
        $user1->password = Hash::make('admin');
        $user1->save();
        $user1->roles()->attach($admin);
        $user1->permissions()->attach($createProducts);
        $user1->permissions()->attach($updateProducts);
        $user1->permissions()->attach($deleteProducts);

        $user2 = new User();
        $user2->login = 'user';
        $user2->password = Hash::make('user');
        $user2->save();
    }
}
