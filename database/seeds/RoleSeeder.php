<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => Role::adminRoleName,
                'slug' => Role::adminRoleSlug
            ], [
                'name' => Role::defaultRoleName,
                'slug' => Role::defaultRoleSlug
            ], [
                'name' => 'Moderator',
                'slug' => 'moderator'
            ], [
                'name' => 'Journalist',
                'slug' => 'journalist'
            ], [
                'name' => 'Blocked',
                'slug' => 'blocked'
            ]
        ]);
    }
}
