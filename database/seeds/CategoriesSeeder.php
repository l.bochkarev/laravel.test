<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Women\'s clothing'
            ], [
                'name' => 'Men\'s clothing'
            ], [
                'name' => 'Mobile phones and accessories'
            ], [
                'name' => 'Mother and child'
            ], [
                'name' => 'Cars and motorbikes'
            ], [
                'name' => 'Jewelry and accessories'
            ], [
                'name' => 'Watch'
            ], [
                'name' => 'Computer and office equipment'
            ], [
                'name' => 'Luggage and bags'
            ], [
                'name' => 'Home and garden'
            ], [
                'name' => 'Electronics'
            ], [
                'name' => 'Beauty and Health'
            ], [
                'name' => 'Sports and entertainment'
            ], [
                'name' => 'Shoes'
            ], [
                'name' => 'Lamps and lighting'
            ], [
                'name' => 'Toys and hobbies'
            ], [
                'name' => 'Weddings and events'
            ], [
                'name' => 'Furniture'
            ], [
                'name' => 'Electronic components and accessories'
            ], [
                'name' => 'Products'
            ], [
                'name' => 'Stationery for office and home'
            ], [
                'name' => 'Home appliances'
            ], [
                'name' => 'Home Improvement'
            ], [
                'name' => 'Themed clothes and uniforms'
            ], [
                'name' => 'Hairpieces and Wigs'
            ], [
                'name' => 'Tools'
            ], [
                'name' => 'Security and Protection'
            ], [
                'name' => 'Clothing accessories'
            ], [
                'name' => 'Underwear and pajamas'
            ]
        ]);
    }
}
