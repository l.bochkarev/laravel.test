<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'Create products',
                'slug' => 'create-products'
            ], [
                'name' => 'Update products',
                'slug' => 'update-products'
            ], [
                'name' => 'Delete products',
                'slug' => 'delete-products'
            ]
        ]);
    }
}
