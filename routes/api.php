<?php

Route::middleware('response.json')->name('api.')->group(function () {
    Route::post('users/login', 'API\AuthController@login')->name('users.login');
    Route::post('users/register', 'API\AuthController@register')->name('users.register');
    Route::get('users/me', 'API\AuthController@me')->name('users.me');
    Route::get('users/logout', 'API\AuthController@logout')->name('users.logout');
    Route::get('users/refresh', 'API\AuthController@refresh')->name('users.refresh');

    Route::get('products/index', 'API\ProductController@index')->name('products.index');
    Route::get('products/show/{id}', 'API\ProductController@show')->name('products.show');
    Route::post('products/create', 'API\ProductController@create')->name('products.create');
    Route::post('products/update/{id}', 'API\ProductController@update')->name('products.update');
    Route::delete('products/destroy/{id}', 'API\ProductController@destroy')->name('products.destroy');
    Route::get('products/categories', 'API\ProductController@categories')->name('products.categories');

    Route::post('feedbacks/create/{id}', 'API\FeedbackController@create')->name('feedbacks.create');
});
