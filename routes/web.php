<?php

Route::get('/', 'ProductController@index')->name('/');

Route::resource('products', 'ProductController');

Route::get('users/login', 'UserController@login')->name('users.login');
Route::post('users/postLogin', 'UserController@postLogin')->name('users.postLogin');

Route::get('users/register', 'UserController@register')->name('users.register');
Route::post('users/postRegister', 'UserController@postRegister')->name('users.postRegister');

Route::get('users/logout', 'UserController@logout')->name('users.logout');

Route::post('feedbacks/create', 'FeedbackController@create')->name('feedbacks.create');
